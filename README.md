|                  | INFORMATIONS  | 
| ---------------- | :----------------------------------------------------------: |
|ANDROID MIN SDK     | 16 |
|ANDROID TARGET SDK  | 32 |

## A cosa serve SaveInfo?


SaveInfo è una libreria che permette alle applicazioni Android di registrare e mantenere traccia della activity e i dati che un utente visualizza/inserisce. Questa libreria utilizza [ACRA](https://github.com/ACRA/acra) per due principali ragioni:

- ACRA permette di rilevare quando avviene un crash, o quando l'applicazione ha un comportamento anomalo salvando le informazioni del device e mantiene una copia dello stack trace.
- ACRA permette inoltre di inviare questi dati attraverso una mail o a un server.

SaveInfo quindi rappresenta un'estensione del servizio che ACRA mette a disposizione per ottenere:

- una struttura JSON, presente in [questa repository di test](https://gitlab.com/conerns/simpleapplicationfortesting/-/blob/fragment_test/test_espresso/primo_test/non_parsed.json)
- un [metodo](https://gitlab.com/conerns/saveinfolibrary/-/blob/master/app/src/main/java/com/andrei/save/SaveInformation.java#L116) che converte il JSON in dei [casi di test Espresso](https://gitlab.com/conerns/simpleapplicationfortesting/-/blob/fragment_test/test_espresso/primo_test/parsed_json.txt). 

Ad ora questi dati vengono trasmessi in modalità trasparente, volendo in seguito aggiungere una fase di anonimizzazione dei dati per rispettare la privacy degli utenti che utilizzano una determinata applicazione Android.

Quando i dati saranno anonimizzati i casi di test saranno poi eseguiti, con i dati nuovi, per verificare se è possibile eseguire la rimozione dei bug presenti sfruttando casi di test privi di informazioni sensibili.

#### Setup

Per poter utilizzare questa libreria senza incorrere in problemi con la libreria ACRA, all'interno del vostro file `AndroidManifest.xml` inserite la seguente istruzione:

```xml
<application 
	android:name="com.andrei.save.ApplicationLevelInformation"
	.
	.
/>
```

In questo modo, se la vostra applicazione presenta una classe a livello Application, essa verrà eseguita in seguito a quella presente nel nuovo `AndroidManifest.xml`
