package com.andrei.save;


import android.app.Activity;
import android.view.View;

import androidx.fragment.app.Fragment;

import org.json.JSONException;

import java.util.Arrays;

import it.unimib.anonymizationTechniques.model.InputException;

public class SaveInformationMiddleMan {
    public SaveInformationMiddleMan(Activity activity, View... viewComponents) throws JSONException, InputException {
        SaveInformation.setInformation(activity, Arrays.asList(viewComponents));
    }
    public SaveInformationMiddleMan(Fragment fragment, View... viewComponents) throws JSONException, InputException {
        SaveInformation.setInformation(fragment, Arrays.asList(viewComponents));
    }

}
