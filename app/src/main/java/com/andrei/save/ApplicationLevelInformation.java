package com.andrei.save;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.MailSenderConfigurationBuilder;
import org.acra.config.ToastConfigurationBuilder;
import org.acra.data.StringFormat;

public class ApplicationLevelInformation extends Application {
    private static final String TAG = ApplicationLevelInformation.class.getSimpleName();
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Log.d(TAG, "attachBaseContext() called, ACRA initialized");

        ACRA.init(this, new CoreConfigurationBuilder()
                // Core configuration:
                .withBuildConfigClass(BuildConfig.class)
                .withReportFormat(StringFormat.JSON)
                .withPluginConfigurations(
                        // Each plugin you chose above can be configured with its builder like this:
                        new ToastConfigurationBuilder()
                                .withText("Farewell")
                                .build(),
                        new MailSenderConfigurationBuilder()
                                .withMailTo("andrei.taraboi@gmail.com")
                                .withReportAsFile(true)
                                .withReportFileName(TAG + " report crash.JSON")
                                //defaults to "<applicationId> Crash Report"
                                .withSubject("CRASH REPORT")
                                //defaults to empty
                                .withBody("The crash comes from" + TAG )
                                .build()
                )
        );
    }

}
