package com.andrei.save;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.unimib.anonymizationTechniques.localSuppression.LocalSuppression;
import it.unimib.anonymizationTechniques.model.InputException;
import it.unimib.anonymizationTechniques.model.StringDomain;

public class ParseJSON {
    static Utils espressoTest = new Utils();
    public ParseJSON(){

    }
    static String parsed(String daEseguireParsing) throws JSONException, InputException {
        StringBuilder singoloCasoDiTest = new StringBuilder();
        JSONObject json = new JSONObject(daEseguireParsing);
        JSONArray arrayFlowActivityEFragment = json.getJSONArray("my_custom_data");
        for ( int i = 0; i < arrayFlowActivityEFragment.length(); i++){
            JSONObject singolaComponenteView = arrayFlowActivityEFragment.getJSONObject(i);
            if(singolaComponenteView.has(espressoTest.ACTIVITY)){
                String[] activityArray = singolaComponenteView.get(espressoTest.ACTIVITY).toString().split("@")[0].split("\\.");
                String activityVisualizzata = activityArray[activityArray.length-1];
                singoloCasoDiTest.append("\n@RunWith(AndroidJUnit4ClassRunner.class)\npublic class ").append(activityVisualizzata).append(espressoTest.TEST).append("{\n");
                singoloCasoDiTest.append("@Rule\npublic ActivityScenarioRule<").append(activityVisualizzata).append("> scenario = new ActivityScenarioRule<>(").append(activityVisualizzata).append(".class);");
                singoloCasoDiTest.append("\n@Test\npublic void method-s_name(){\n\t//method's body\n");
                JSONArray arrayDeiComponenti = singolaComponenteView.getJSONArray("step");
                singoloCasoDiTest.append(eseguireParsingComponentiPerCasiDiTest(arrayDeiComponenti));
                singoloCasoDiTest.append("}\n");
            }
            else if(singolaComponenteView.has(espressoTest.FRAGMENT)){
                String fragmentVisualizzato = singolaComponenteView.get(espressoTest.FRAGMENT).toString().split("\\{")[0];
                singoloCasoDiTest.append("@RunWith(AndroidJUnit4ClassRunner.class)\npublic class ").append(fragmentVisualizzato).append(espressoTest.TEST).append("{\n");
                singoloCasoDiTest.append("@Test\npublic void method-s_name() {\n\t//method's body\n");
                singoloCasoDiTest.append("FragmentScenario<").append(fragmentVisualizzato).append("> scenario = FragmentScenario.launchInContainer(").append(fragmentVisualizzato).append(".class);\n");
                JSONArray arrayDeiComponenti = singolaComponenteView.getJSONArray("step");
                singoloCasoDiTest.append(eseguireParsingComponentiPerCasiDiTest(arrayDeiComponenti));
                singoloCasoDiTest.append("}\n");
            }
        }
        return singoloCasoDiTest.toString();
    }

    static String eseguireParsingComponentiPerCasiDiTest(JSONArray arrayDeiComponenti) throws JSONException, InputException {
        StringBuilder singoloCasoDiTest = new StringBuilder();
        String espressoOnView = espressoTest.espressoOnView;
        for(int j = 0; j < arrayDeiComponenti.length(); j++) {
            String[] parseDelType = arrayDeiComponenti.getJSONObject(j).get("type").toString().split("\\.");
            String id = arrayDeiComponenti.getJSONObject(j).get("id").toString().split("\\/")[1].split("\\}")[0];
            String type = parseDelType[parseDelType.length-1];
            String value ="";
            if(arrayDeiComponenti.getJSONObject(j).has("value"))
                value = (String) arrayDeiComponenti.getJSONObject(j).get("value");
            if(type.contains("Button")) {
                singoloCasoDiTest.append(espressoTest.closeSoftKey);
                if(type.contains("AppCompatToggleButton")) {
                    if(value.equals("true")){
                        singoloCasoDiTest
                                .append(espressoOnView)
                                .append(id)
                                .append(espressoTest.matchChecked)
                                .append(espressoTest.click)
                                .append(espressoTest.fineRiga);
                        singoloCasoDiTest
                                .append(espressoOnView)
                                .append(id)
                                .append(espressoTest.matchNotChecked)
                                .append(espressoTest.fineRiga);
                    }else{
                        singoloCasoDiTest
                                .append(espressoOnView)
                                .append(id)
                                .append(espressoTest.matchNotChecked)
                                .append(espressoTest.fineRiga);
                    }
                }
                else
                    singoloCasoDiTest.append(espressoOnView)
                            .append(id)
                            .append(espressoTest.clickInMezzo)
                            .append(espressoTest.fineRiga);
            }
            if(type.contains("EditText")) {
                if (arrayDeiComponenti.getJSONObject(j).get("value").toString().equals(""))
                    singoloCasoDiTest
                            .append(espressoOnView)
                            .append(id)
                            .append(espressoTest.typeEmpty)
                            .append(espressoTest.fineRiga);
                else{
                    value = arrayDeiComponenti.getJSONObject(j).get("value").toString();
                    StringDomain valueType = new StringDomain("[A-Za-z]", value.length());
                    LocalSuppression<String> technique = new LocalSuppression<>(value, valueType);
                    Object anonymizedByDefinition = technique.anonymize();
                    singoloCasoDiTest
                            .append(espressoOnView)
                            .append(id)
                            .append(espressoTest.typeValueHead)
                            .append(anonymizedByDefinition)
                            .append(espressoTest.typeValueTail)
                            .append(espressoTest.fineRiga);
                }
            }
            if(type.contains("RadioGroup")) {
                singoloCasoDiTest
                        .append(espressoTest.espressoOnMatch)
                        .append('"' +value)
                        .append(espressoTest.typeValueTail)
                        .append(espressoTest.checkMatchedDisplay)
                        .append(espressoTest.click)
                        .append(espressoTest.fineRiga);
            }
            if(type.contains("AppCompatSpinner")){
                singoloCasoDiTest
                        .append(espressoOnView)
                        .append(id)
                        .append(espressoTest.click)
                        .append(espressoTest.fineRiga);
                singoloCasoDiTest
                        .append("Espresso.onData(allOf(is(instanceOf(String.class)), is(" + '"' +value +'"' + "))).perform(click());\n");
            }if(type.contains("CheckBox")) {
                if (value.equals("true")) {
                    singoloCasoDiTest
                            .append(espressoOnView)
                            .append(id)
                            .append(espressoTest.matchNotChecked)
                            .append(espressoTest.click)
                            .append(espressoTest.fineRiga);
                    singoloCasoDiTest.append(espressoOnView)
                            .append(id)
                            .append(espressoTest.matchChecked)
                            .append(espressoTest.fineRiga);
                } else
                    singoloCasoDiTest
                            .append(espressoOnView)
                            .append(id)
                            .append(espressoTest.matchNotChecked)
                            .append(espressoTest.fineRiga);
            }
        }
        return singoloCasoDiTest.toString();
    }

}
