package com.andrei.save;

public class Utils {

    protected final String INSERTED_VALUES = "inserted_values";
    protected final String PARSED_VALUES = "parsed_values";
    protected final String ACTIVITY = "activity";
    protected final String FRAGMENT = "fragment";
    protected  final String NO_PARSED = "NO_PARSED";
    protected final String PARSED = "PARSED";
    protected final String TEST = "Test";
    protected final String espressoOnView = "Espresso.onView(ViewMatchers.withId(R.id.";
    protected final String closeSoftKey = "Espresso.closeSoftKeyboard();n";
    protected final String matchChecked = ")).check(matches(isChecked()))";
    protected final String click = ".perform(click())";
    protected final String clickInMezzo = ")).perform(ViewActions.click())";
    protected final String matchNotChecked = " )).check(matches(isNotChecked()))";
    protected final String typeEmpty = ")).perform(replaceText(" + '"' + '"' + "))";
    protected final String typeValueHead = ")).perform(replaceText(" + '"';
    protected final String typeValueTail = '"' + "))";
    protected final String espressoOnMatch = "Espresso.onView(ViewMatchers.withText(";
    protected final String checkMatchedDisplay = "check(matches(isDisplayed()))";
    protected final String fineRiga =";\n";
    public Utils(){
    }

}
