package com.andrei.save;
import static com.andrei.save.ParseJSON.eseguireParsingComponentiPerCasiDiTest;
import static com.andrei.save.ParseJSON.parsed;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import it.unimib.anonymizationTechniques.model.InputException;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.fragment.app.Fragment;
import com.google.android.material.textfield.TextInputEditText;
import org.acra.ACRA;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class SaveInformation {
    static Utils espressoTest = new Utils();

    private static LinkedHashMap<String, Integer> contaStep = new LinkedHashMap <String, Integer>();
    static JSONArray myCustomData = new JSONArray();
    static JSONObject activity = new JSONObject();
    static JSONArray steps = new JSONArray();
    static String forAcra = "{\"my_custom_data\":[";

    public static void SaveInformation(Activity currActivity, List<View> components) {
    }

    public static void setInformation(Activity currentViewPassed, List<View> components) throws JSONException, InputException {
        activity.put(espressoTest.ACTIVITY, currentViewPassed.toString());
        setInformationForActivityAndFragment(activity.toString(), components);
    }
    public static void setInformation(Fragment currentViewPassed, List<View> components) throws JSONException, InputException {
        activity.put(espressoTest.FRAGMENT, currentViewPassed.toString());
        setInformationForActivityAndFragment(currentViewPassed.toString(), components);
    }

    private static void setInformationForActivityAndFragment(String currentViewPassed, List<View> components) throws JSONException, InputException {
        putInformationInside(currentViewPassed, components);
        creazioneStepActivityOFragment(currentViewPassed, components);
        Log.d("ACRA", forAcra);
        String strutturaJson = new JSONObject(forAcra.substring(0,forAcra.length()-1)+"]}").toString();
        Log.d(espressoTest.NO_PARSED, strutturaJson);
        Log.d(espressoTest.PARSED, ParseJSON.parsed(strutturaJson));
        ACRA.getErrorReporter().putCustomData(espressoTest.INSERTED_VALUES,strutturaJson);
        ACRA.getErrorReporter().putCustomData(espressoTest.PARSED_VALUES,parsed(strutturaJson));
    }
    /**
     * Questo metodo aggiorna i step dell'applicazione in modo da poter salvare [step0, step1], ad ora questo metodo è
     * non utilizzato facendo solamente uso di [step].
     *
     * @param currentViewPassed questo parametro fa riferimento alla view visualizzata dall'utente prima di eseguire
     *                          il cambio Activity/Fragment
     * @param components è la lista dei componenti presenti sull currentViewPassed
     * @deprecated
     * */
    private static void putInformationInside(String currentViewPassed, List<View> components) throws JSONException {
        if(!contaStep.isEmpty()) {
            Object o = contaStep.entrySet().toArray()[contaStep.size() - 1];
            o.toString();
            if (o.toString().startsWith(currentViewPassed.toString())) {
                int valore_attuale = contaStep.get(currentViewPassed);
                contaStep.put(currentViewPassed.toString(), (valore_attuale + 1));
            } else
                contaStep.put(currentViewPassed, 0);
        }else
            contaStep.put(currentViewPassed, 0);
    }
    //qiu si possono far ritornare le Rule invece delle Activity flow






    private static void creazioneStepActivityOFragment(String currentViewPassed, List<View> components) throws JSONException {
        List<JSONObject> listaComponentiPerStep = new ArrayList<>();
        for(View elemento: components){
            JSONObject singoloComponente = new JSONObject();

            /** corrispetitivi { "type" : <componentType>, "id": <componentId>, "value": <componentValue>} presenti nel file
             * json per la maggior parte dei componenti.*/
            String type =  elemento.toString().split(Pattern.quote("{"))[0];
            String[] id = elemento.toString().split(Pattern.quote(":"))[1].split(" ");
            String value = "";
            if ( elemento instanceof TextInputEditText)
                value = Objects.requireNonNull(((TextInputEditText) elemento).getText()).toString();
            if ( elemento instanceof EditText)
                value = Objects.requireNonNull(((EditText) elemento).getText()).toString();
            if( elemento instanceof Button)
                value =  ((Button) elemento).getText().toString();
            if(elemento instanceof AppCompatToggleButton)
                value = String.valueOf(((AppCompatToggleButton) elemento).isChecked());
            if(elemento instanceof RadioGroup) {
                RadioButton rb = elemento.findViewById(((RadioGroup) elemento).getCheckedRadioButtonId());
                value =  rb.getText().toString();
            }
            if(elemento instanceof AppCompatSpinner)
                value = Objects.requireNonNull(((AppCompatSpinner) elemento).getSelectedItem()).toString();
            if(elemento instanceof ProgressBar) {
                value = ((ProgressBar) elemento).getProgress() +"";
            }
            if(elemento instanceof AutoCompleteTextView)
                value = Objects.requireNonNull(((AutoCompleteTextView) elemento).getText()).toString();
            if(elemento instanceof CheckBox) {
                value = ((CheckBox) elemento).isChecked() +"";
            }
            singoloComponente.put("id", id[0]);
            singoloComponente.put("type", type);
            singoloComponente.put("value", value);
            listaComponentiPerStep.add(singoloComponente);
        }
        JSONArray list = new JSONArray(listaComponentiPerStep);
        String singolo_step = "step";
        activity.put(singolo_step, list);
        forAcra += activity + ",";
        myCustomData.put(activity);
        activity = new JSONObject();
        steps = new JSONArray();

    }

}
